﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LambdaDojo
{
    class Level1
    {

        public static void Exec()
        {
            Level1 level = new Level1();

            level.ToStringInt();
            level.ConsoleAlert();
            level.Multiply();
        }
        
        public void ToStringInt()
        {
            Func<int, string> toStringInt = null;

            // Ecrire la fonction toStringInt en lambda (retourne un int sous forme de string)

            Console.WriteLine("ToStringInt : " + toStringInt(5));
        }

        public void ConsoleAlert()
        {
            Action consoleAlert = null;

            // Ecrire la méthode consoleAlert (écrit "ALERTE" dans la console)

            consoleAlert();
        }

        public void Multiply()
        {            
            Func<int, int, int> multiply = null;
                      
            // Ecrire la fonction multiply en lambda

            Console.WriteLine("Multiply result : 2 * 3 = " + multiply(2, 3));
        }
        
    }
}
