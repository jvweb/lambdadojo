﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LambdaDojo
{
    class Delegates
    {

        delegate string Test1(int a);

        delegate void Test2(string a);

        delegate string MethodDelegate();

        
        public string ThisIsMyFirstMethod(int number)
        {
            return number.ToString();
        }

        public void ThisIsMySecondMethod(string input)
        {
            Console.WriteLine(input);
        }

        public void UsingDelegates()
        {
            // On peut utiliser des variables du type créé par les delegates pour stocker les méthodes à utiliser par la suite
            Test1 myMethod1 = ThisIsMyFirstMethod;
            Test2 myMethod2 = ThisIsMySecondMethod;

            // On peut ensuite appeler les méthodes comme suit
            string result = myMethod1(3);
            myMethod2("this is input");
        }

        public void UsingFuncAndActions()
        {
            Func<int, string> myMethod1 = ThisIsMyFirstMethod;
            Action<string> myMethod2 = ThisIsMySecondMethod;

            // On peut ensuite appeler les méthodes comme suit
            string result = myMethod1(3);
            myMethod2("this is input");
        }

        public void UsingFuncAndActionsLambda()
        {
            Func<int, string> myMethod1 = x => x.ToString();
            Action<string> myMethod2 = x => Console.WriteLine(x);

            // On peut ensuite appeler les méthodes comme suit
            string result = myMethod1(3);
            myMethod2("this is input");
        }

        public void UsingFuncAndActionsLambdaMultiple()
        {
            Func<int, int, string> myMethod1 = (x, y) => x.ToString() + "/" + y.ToString();
                                    
            string result = myMethod1(1, 20); // result = "1/20"
        }





    }
}
